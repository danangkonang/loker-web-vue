import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { RootState } from './types';

import { actions } from './actions';
import { mutations } from './mutations';

import { academy } from './academy/index';
import { job } from './job/index';
import { misc } from './misc/index';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    academy,
    job,
    misc,
  },
  state: {
    layout: 'content-layout',
    locale: 'id',
  },
  actions,
  mutations,
};

export default new Vuex.Store<RootState>(store);
