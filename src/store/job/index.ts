import { Module } from 'vuex';

import { RootState } from '../types';
import { JobState } from './types';

import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';

import { JobVacancyModel } from '@/models/job.model';
import { defaultPagination } from '@/functions/pagination.function';

export const state: JobState = {
  job_search: {
    category: null,
    job_title: null,
  },
  vacancy: new JobVacancyModel(),
  pagination_vacancy: defaultPagination,
  array_job_category: new Array(),
  array_job_specialization: new Array(),
  array_job_vacancy: new Array(),
};

const namespaced: boolean = true;

export const job: Module<JobState, RootState> = {
  namespaced,
  state,
  actions,
  getters,
  mutations,
};
