import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { JobState } from './types';

export const getters: GetterTree<JobState, RootState> = {
  arrayCategory(state): any[] {
    state.array_job_category.sort(
      (a, b) => {
        return (a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0));
      },
    );
    return state.array_job_category;
  },
  arraySpecialization(state): any[] {
    state.array_job_specialization.sort(
      (a, b) => {
        return (a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0));
      },
    );
    return state.array_job_specialization;
  },
};
