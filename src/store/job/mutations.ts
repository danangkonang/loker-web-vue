import { MutationTree } from 'vuex';
import { JobState } from './types';
import { JobVacancy } from '@/models/job.types';

export const mutations: MutationTree<JobState> = {
  mutationVacancy(state, payload: JobVacancy) {
    state.vacancy.setModelFromObject(payload);
  },
};
