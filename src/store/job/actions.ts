import { ActionTree } from 'vuex';

import { RootState } from '../types';
import { JobState } from './types';

import toast from '@/functions/toast.function';
import { createPagination } from '@/functions/pagination.function';

import JobService from '@/services/job.service';
import PaginationService from '@/services/pagination.service';

const serviceJob: JobService = new JobService();
const servicePagination: PaginationService = new PaginationService();

export const actions: ActionTree<JobState, RootState> = {
  actionSelectUnpagedJobCategory({commit, state}) {
    serviceJob.serviceSelectUnpagedJobCagetory().then((response: any) => {
      if (response) {
        state.array_job_category = response.data;
      }
    });
  },
  actionSelectUnpagedJobTitle({commit, state}) {
    serviceJob.serviceSelectUnpagedJobTitle().then((response: any) => {
      if (response) {
        state.array_job_specialization = response.data;
      }
    });
  },
  actionSelectJobVacancy({commit, state}) {
    const params: any = Object.assign({}, state.job_search);
    if (params.category) {
      params.category = params.category.id;
    }
    if (params.job_title) {
      params.job_title = params.job_title.id;
    }
    serviceJob.serviceSelectVacancyList(params).then((response: any) => {
      if (response) {
        state.array_job_vacancy = response.data.results;
        state.pagination_vacancy = createPagination(response.data);
      }
    });
  },
  actionPaginationVacancy({commit, state}, payload: any) {
    const params: any = Object.assign({}, state.job_search);
    if (params.category) {
      params.category = params.category.id;
    }
    if (params.job_title) {
      params.job_title = params.job_title.id;
    }
    servicePagination.servicePaginate(payload.url, payload.page, params).then((response: any) => {
      if (response) {
        state.array_job_vacancy = response.data.results;
        state.pagination_vacancy = createPagination(response.data);
      }
    });
  },
  actionSelectJobVacancyById({commit, state}) {
    serviceJob.serviceSelectJobVacancyById(state.vacancy.id).then((response: any) => {
      if (response) {
        state.vacancy.setModelFromObject(response.data);
      }
    });
  },
};
