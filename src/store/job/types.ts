import { JobVacancy } from '@/models/job.types';
import { PaginationState } from '../types';

export interface JobState {
  job_search: any;
  vacancy: JobVacancy;
  pagination_vacancy: PaginationState;
  array_job_category: any[];
  array_job_specialization: any[];
  array_job_vacancy: JobVacancy[];
}
