import { ActionTree } from 'vuex';
import { RootState } from './types';

export const actions: ActionTree<RootState, RootState> = {
  mainLayout({commit}) {
    commit('mutationLayout', 'main-layout');
  },
  contentLayout({commit}) {
    commit('mutationLayout', 'content-layout');
  },
};
