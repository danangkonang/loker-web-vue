import { ActionTree } from 'vuex';

import { RootState } from '../types';
import { MiscState } from './types';

import toast from '@/functions/toast.function';

import MiscService from '@/services/misc.service';

const serviceMisc: MiscService = new MiscService();

export const actions: ActionTree<MiscState, RootState> = {
  actionSelectStatistic({commit, state}) {
    serviceMisc.serviceSelectMisc().then((response: any) => {
      if (response) {
        let data = response.data;

        // Dummpy calculation for pretty number
        state.statistic.activeJobs = data.activeJobs + 330;
        state.statistic.registeredSchool = 0;
        state.statistic.partnerCompanies = data.partnerCompanies + 103;
        state.statistic.registeredCandidates = data.registeredCandidates + 2880;
      }
    });
  },
  actionSelectUpagedCity({commit, state}) {
    serviceMisc.serviceSelectUpagedCity().then((response: any) => {
      if (response) {
        state.array_city = response.data;
      }
    });
  },
  actionSendMail({commit,state}){
    const params = Object.assign({} , state.contact_us)
    serviceMisc.serviceSendMail(params).then((response:any)=>{
      if(response.status){
        toast.success(response.status)
      }
      state.contact_us.sender = '';
      state.contact_us.subject = '';
      state.contact_us.body = '';
    })
  }
};
