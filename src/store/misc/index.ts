import { Module } from 'vuex';

import { RootState } from '../types';
import { MiscState } from './types';

import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';

import { JobVacancyModel } from '@/models/job.model';

export const state: MiscState = {
  statistic: {
    activeJobs: 0,
    registeredSchool: 0,
    partnerCompanies: 0,
    registeredCandidates: 0,
  },
  contact_us: {
    sender : null,
    subject : null,
    body : null,
  },
  array_city: new Array(),
};

const namespaced: boolean = true;

export const misc: Module<MiscState, RootState> = {
  namespaced,
  state,
  actions,
  getters,
  mutations,
};
