import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { MiscState } from './types';

export const getters: GetterTree<MiscState, RootState> = {
  arrayCity(state): any[] {
    state.array_city.sort(
      (a, b) => {
        return (a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0));
      },
    );
    return state.array_city;
  },
};
