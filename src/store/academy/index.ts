import { Module } from 'vuex';

import { RootState } from '../types';
import { AcademyState } from './types';

import { actions } from './actions';
import { mutations } from './mutations';

import { defaultPagination } from '@/functions/pagination.function';
import { SchoolSearchModel } from '@/models/academy.model';

export const state: AcademyState = {
  school_search: new SchoolSearchModel(),
  array_academy: new Array(),
  pagination_academy: defaultPagination,
};

const namespaced: boolean = true;

export const academy: Module<AcademyState, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
};
