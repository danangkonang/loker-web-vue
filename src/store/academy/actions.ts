import { ActionTree } from 'vuex';

import { RootState } from '../types';
import { AcademyState } from './types';

import toast from '@/functions/toast.function';
import { createPagination } from '@/functions/pagination.function';

import AcademyService from '@/services/academy.service';
import PaginationService from '@/services/pagination.service';

const serviceAcademy: AcademyService = new AcademyService();
const servicePagination: PaginationService = new PaginationService();

export const actions: ActionTree<AcademyState, RootState> = {
  actionSelectAcademyList({commit, state}) {
    const params: any = Object.assign({}, state.school_search);
    if (params.city) {
      params.city = params.city.id;
    }
    serviceAcademy.serviceSchoolList(params).then((response: any) => {
      if (response) {
        state.array_academy = response.data.results;
        state.pagination_academy = createPagination(response.data);
      }
    });
  },
  actionPaginationAcademyList({commit, state}, payload: any) {
    const params: any = Object.assign({}, state.school_search);
    if (params.city) {
      params.city = params.city.id;
    }
    servicePagination.servicePaginate(payload.url, payload.page, params).then((response: any) => {
      if (response) {
        state.array_academy = response.data.results;
        state.pagination_academy = createPagination(response.data);
      }
    });
  },
};
