import { PaginationState } from '../types';
import { SchoolSearch } from '@/models/academy.types';

export interface AcademyState {
  school_search: SchoolSearch;
  array_academy: any;
  pagination_academy: PaginationState;
}
