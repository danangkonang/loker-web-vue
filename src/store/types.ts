export interface RootState {
  layout: string;
  locale: string;
}

export interface PaginationState {
  count: number;
  url: any;
}
