import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import './registerServiceWorker';

import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
import i18n from './i18n';

import AOS from 'aos';

AOS.init();

Vue.component('v-select', vSelect);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
