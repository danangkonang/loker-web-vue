import { JobPosition, JobVacancy, JobDetail, JobRequirement } from './job.types';
import { BaseModel } from './bases/base.model';

export class JobPositionModel extends BaseModel implements JobPosition {
  public id = 0;
  public name = '';
  public translation = '';

  constructor() {
    super();
  }
}

export class JobDetailModel extends BaseModel implements JobDetail {
  public id  = 0;
  public company = null;
  public city = null;
  public employment_type = null;
  public expect_salary_from = '';
  public expect_salary_to = '';
  public job_specialization = null;
  public position = null;

  constructor() {
    super();
  }
}

export class JobRequirementModel extends BaseModel implements JobRequirement {
  public id = 0;
  public education_level = null;
  public gpa = 0;
  public job_desc = '';
  public language = null;
  public major = null;
  public personality = null;
  public skill = null;
  public year_exp = 0;
  public year_exp_start = 0;
  public year_exp_end = 0;

  constructor() {
    super();
  }
}

export class JobVacancyModel extends BaseModel implements JobVacancy {
  public id = 0;
  public detail = new JobDetailModel();
  public requirement = new JobRequirementModel();
  public suspension = false;
  public suspension_reason = '';
  public report = false;

  constructor() {
    super();
  }
}
