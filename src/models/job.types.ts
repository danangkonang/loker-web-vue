import { Base } from './bases/base.types';

export interface JobPosition extends Base {
  id: number;
  name: string;
  translation: string;
}

export interface JobDetail extends Base {
  id: number;
  company: null;
  city: null;
  employment_type: null;
  expect_salary_from: string | number;
  expect_salary_to: string | number;
  job_specialization: null;
  position: null;
}

export interface JobRequirement extends Base {
  id: number;
  education_level: null;
  gpa: number;
  job_desc: string;
  language: null;
  major: null;
  personality: null;
  skill: null;
  year_exp: number;
  year_exp_end: number;
  year_exp_start: number;
}

export interface JobVacancy extends Base {
  id: number;
  detail: JobDetail;
  requirement: JobRequirement;
  suspension: boolean;
  suspension_reason: string;
  report: boolean;
}
