import { Base } from './base.types';

export class BaseModel implements Base {
  public created = null;
  public updated = null;

  public setModelFromObject(data: any) {
    if (typeof data === 'string') {
      data = JSON.parse(data);
    }
    for (const key in this) {
      if (data[key]) {
        this[key] = data[key];
      }
    }
  }
}
