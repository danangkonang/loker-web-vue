export interface Base {
  created: Date | string | null;
  updated: Date | string | null;
  setModelFromObject: (data: any) => void;
}
