import { Base } from './bases/base.types';

export interface SchoolSearch extends Base {
  city: string;
  school_name: string;
}

export interface SchoolType extends Base {
  id: number;
  name: string;
  translation: string;
}

export interface DomainChekcer extends Base {
  is_valid: boolean;
  description: string;
}

export interface School extends Base {
  id: number;
  school_type: SchoolType | number;
  name: string;
  branch: string;
  phone: string;
  email: string;
  fax: string;
  address: string;
  postal_code: string;
  website: string;
  subdomain: string;
  logo: any;
  logo_url: string;
  banner: any;
  is_registered: boolean;
  agreement: any;
  about: string;
  vision: string;
  total_revenue: number;
}

export interface SchoolPersonal extends Base {
  id: number;
  first_name: string;
  last_name: string;
  position: string;
  phone: string;
  email: string;
  first_admin: boolean;
  school?: School | number;
}
