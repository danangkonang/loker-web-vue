import toast from '@/functions/toast.function';
import { BaseModel } from './bases/base.model';

import {
  SchoolType, School, SchoolPersonal, SchoolSearch,
} from './academy.types';

export class SchoolSearchModel extends BaseModel implements SchoolSearch {
  public city = '';
  public school_name = '';
}

export class SchoolTypeModel extends BaseModel implements SchoolType {
  public id = 0;
  public name = '';
  public translation = '';

  constructor() {
    super();
  }
}

export class DomainChekcerModel extends BaseModel {
  public is_valid = false;
  public description = '';

  constructor() {
    super();
  }
}

export class SchoolModel extends BaseModel implements School {
  public id = 0;
  public school_type = 0;
  public name = '';
  public branch = '';
  public city = null;
  public phone = '';
  public email = '';
  public fax = '';
  public address = '';
  public postal_code = '';
  public website = '';
  public subdomain = '';
  public logo = null;
  public logo_url = '';
  public banner = null;
  public is_registered = false;
  public agreement = null;
  public about = '';
  public vision = '';
  public total_revenue = 0;

  constructor() {
    super();
  }
}

export class SchoolPersonalModel extends BaseModel implements SchoolPersonal {
  public id = 0;
  public first_name = '';
  public last_name = '';
  public position = '';
  public phone = '';
  public email = '';
  public school = 0;
  public user = 0;
  public first_admin = false;

  constructor() {
    super();
  }
}
