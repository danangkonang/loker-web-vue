/* apiBase - START */
// Description : apiBase is for controller of baseAPI when production or development.
// Like anticipation for forgot change baseAPI when production build.
// Author : Komang Suryadana.
// Created on : Tue, 29 January 2019.   Updated on : Tue, 29 January 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

/* const isProduction = process.env.NODE_ENV === 'production' */
/* export const baseAPI = isProduction ? 'https://api-cs.jaqun.com/api/v1/' : 'http://192.168.5.171:9880/api/v1/' */
// export const baseAPI = 'http://45.118.134.76:9880/api/v1/';
export const baseAPI = 'https://backend.career.support/api/v1/';
// export const baseAPI = 'http://192.168.5.213:8000/api/v1/';
// export const baseAPI = 'http://127.0.0.1:8000/api/v1/';

/* apiBase - END */
