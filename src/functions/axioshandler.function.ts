import { SUCCESS_STATUS_CODE } from '@/constants/response.constant';
import toast from './toast.function';

/* axiosHandler - START */
// Description : axiosHandler is collection function for handling request, response, and error handlers.
// Author : Komang Suryadana.
// Created on : Tue, 29 January 2019.   Updated on : Tue, 29 January 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

function capitalize(value: string) {
  return value.charAt(0).toUpperCase() + value.slice(1);
}

export function errorResponseHandler(error: any) {
  // Declare custome BreakException for breaking array foreach function
  const BreakException = {};

  if (error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false) {
    return Promise.reject(error);
  }

  if (error.response) {

    const response = error.response;

    if (response.status === 401) {
      // clearToken();
      return;
    }

    // if type data is array
    if (Array.isArray(response.data)) {
      try {
        Array.from(response.data).forEach((item) => {
          if (item === 'Signature has expired.') {
            toast.error('Session has expired.');
            throw BreakException;
          }
          toast.error(item);
        });
      } catch (e) {
        // When you need more debuging you can print exception
      }
      return;
    }

    // If type data is object
    for (const key in response.data) {
      if (key) {
        // If type data is string
        if (typeof (response.data[key]) === 'string') {
          const keyField = capitalize(key.replace('_', ' '));
          if (response.data[key] === 'Signature has expired.') {
            toast.error('Session has expired.');
            break;
          }
          toast.error(`Field ${keyField}, ${response.data[key]}`);
          continue;
        }

        if (Array.isArray(error.response.data[key])) {
          try {
            Array.from(response.data[key]).forEach((item) => {
              if (item === 'Signature has expired.') {
                toast.error('Session has expired.');
                throw BreakException;
              }
              const keyField = capitalize(key.replace('_', ' '));
              toast.error(`Field ${keyField}, ${item}`);
            });
          } catch (e) {
            // When you need more debuging you can print exception
          }
        }
      }
    }
  }
}

export function responseHandler(response: any) {
  if (SUCCESS_STATUS_CODE.includes(response.status)) {
    return response;
  }
  return null;
}

export function requestHandler(config: any) {
  return config;
}

/* axiosHandler - END */
