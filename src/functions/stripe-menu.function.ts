/* tslint:disable */
export const Util = {
  queryArray: (e: any, p: any) => {
    p || (p = document.body);
    return Array.prototype.slice.call(p.querySelectorAll(e));
  },
  touch: {
    isSupported: 'ontouchstart' in global || navigator.maxTouchPoints,
    isDragging: false,
  },
};

export class StripeMenu {
  private events: any;
  private container: any;
  private root: any;
  private dropdownBackground: any;
  private dropdownBackgroundAlt: any;
  private dropdownContainer: any;
  private dropdownArrow: any;
  private dropdownSection: any;
  private dropdownSections: any[];
  private hasDropdownLinks: any[];
  private activeDropdown: any;
  private enableTransitionTimeout: any;
  private disableTransitionTimeout: any;
  private closeDropdownTimeout: any;

  public constructor(menuElement: any) {

    // Main events used to enable interaction with menu


    this.events = (window as any).PointerEvent ? {
        end: 'pointerup',
        enter: 'pointerenter',
        leave: 'pointerleave',
    } : {
        end: 'touchend',
        enter: 'mouseenter',
        leave: 'mouseleave',
    };


    // The main navigation element.

    this.container = document.querySelector(menuElement);
    this.container.classList.add('no-dropdown-transition');


    // Element holding the menu options, not the dropdown

    this.root = this.container.querySelector('.st-nav-menu');


    // Those elements used to show the dropdown animation and transitioning

    this.dropdownBackground = this.container.querySelector('.st-dropdown-bg');
    this.dropdownBackgroundAlt = this.container.querySelector('.st-alt-bg');
    this.dropdownContainer = this.container.querySelector('.st-dropdown-container');
    this.dropdownArrow = this.container.querySelector('.st-dropdown-arrow');


    // Elements which will have the dropdown content to be shown

    this.hasDropdownLinks = Util.queryArray('.st-has-dropdown', this.root);


    // Each dropdown section to be displayed on mouse interactions

    this.dropdownSections = Util.queryArray('.st-dropdown-section', this.container).map((el: any) => {
        return {
            el,
            name: el.getAttribute('data-dropdown'),
            content: el.querySelector('.st-dropdown-content'),
            width: el.querySelector('.st-dropdown-content').offsetWidth,
        };
    });

    // Menu link interaction

    this.hasDropdownLinks.forEach((el: any) => {
      el.addEventListener(this.events.enter, (evt: any) => {
        /* tslint:disable */
        if (evt.pointerType === 'touch') return;
        /* tslint:enable */
        this.stopCloseTimeout();
        this.openDropdown(el);
      });

      el.addEventListener(this.events.leave, (evt: any) => {
        /* tslint:disable */
        if (evt.pointerType === 'touch') return;
        /* tslint:enable */
        this.startCloseTimeout();
      });

      el.addEventListener(this.events.end, (evt: any) => {
        evt.preventDefault();
        evt.stopPropagation();
        this.toggleDropdown(el);
      });
    });

    // Menu container interaction with content

    this.dropdownContainer.addEventListener(this.events.enter, (evt: any) => {
      /* tslint:disable */
      if (evt.pointerType === 'touch') return;
      /* tslint:enable */
      this.stopCloseTimeout();
    });

    this.dropdownContainer.addEventListener(this.events.leave, (evt: any) => {
      /* tslint:disable */
      if (evt.pointerType === 'touch') return;
      /* tslint:enable */
      this.startCloseTimeout();
    });

    this.dropdownContainer.addEventListener(this.events.end, (evt: any) => {
        evt.stopPropagation();
    });

    document.body.addEventListener(this.events.end, (e: any) => {
      /* tslint:disable */
      Util.touch.isDragging || this.closeDropdown();
      /* tslint:enable */
    });
  }

  private openDropdown(hasDropDownLink?: any) {
    /* tslint:disable */
    if (this.activeDropdown === hasDropDownLink) return;
    /* tslint:enable */

    this.activeDropdown = hasDropDownLink;

    this.container.classList.add('overlay-active');
    this.container.classList.add('dropdown-active');

    // Setting the default menu active equals to this link

    this.hasDropdownLinks.forEach((link: any) => {
        link.classList.remove('active');
    });
    hasDropDownLink.classList.add('active');

    // Next section to show

    const nextSection = hasDropDownLink.getAttribute('data-dropdown');
    let position = 'left';

    let dropdown = {
        width: 0,
        height: 0,
        content: null,
    };

    this.dropdownSections.forEach((dropDownSection) => {
        dropDownSection.el.classList.remove('active');
        dropDownSection.el.classList.remove('left');
        dropDownSection.el.classList.remove('right');

        if (dropDownSection.name === nextSection) {
            dropDownSection.el.classList.add('active');
            position = 'right';

            dropdown = {
                width: dropDownSection.content.offsetWidth,
                height: dropDownSection.content.offsetHeight,
                content: dropDownSection.content,
            };
        } else {
            dropDownSection.el.classList.add(position);
        }
    });

    const u = 520;
    const a = 400;
    const ddCr = hasDropDownLink.getBoundingClientRect();
    const scaleX = dropdown.width / u;
    const scaleY = dropdown.height / a;
    let translateX = ddCr.left + ddCr.width / 2 - dropdown.width / 2;

    translateX = Math.round(Math.max(translateX, 10));

    clearTimeout(this.disableTransitionTimeout);
    this.enableTransitionTimeout = setTimeout(() => {
        this.container.classList.remove('no-dropdown-transition');
    }, 50);

    this.dropdownBackground.style.transform = ('translateX(' + translateX + 'px) scaleX(' +
    scaleX + ') scaleY(' + scaleY + ')');
    this.dropdownContainer.style.transform = 'translateX(' + translateX + 'px)';

    this.dropdownContainer.style.width = dropdown.width + 'px';
    this.dropdownContainer.style.height = dropdown.height + 'px';

    const arrowPosX = Math.round(ddCr.left + ddCr.width / 2);
    this.dropdownArrow.style.transform = 'translateX(' + arrowPosX + 'px) rotate(45deg)';

    const d = (dropdown as any).content.children[0].offsetHeight / scaleY;
    this.dropdownBackgroundAlt.style.transform = 'translateY(' + d + 'px)';
  }

  private closeDropdown() {
    /* tslint:disable */
    if (!this.activeDropdown) return;
    /* tslint:enable */

    this.hasDropdownLinks.forEach((link: any, t: any) => {
      link.classList.remove('active');
    });

    clearTimeout(this.enableTransitionTimeout);

    this.disableTransitionTimeout = setTimeout(() => {
      this.container.classList.add('no-dropdown-transition');
    }, 50);

    this.container.classList.remove('overlay-active');
    this.container.classList.remove('dropdown-active');
    this.activeDropdown = undefined;
  }

  private toggleDropdown(e: any) {
    this.activeDropdown === e ? this.closeDropdown() : this.openDropdown();
  }

  private startCloseTimeout() {
    this.closeDropdownTimeout = setTimeout(() => {
      this.closeDropdown();
    }, 50);
  }

  private stopCloseTimeout() {
    clearTimeout(this.closeDropdownTimeout);
  }
}

export class StripeMenuPopup {
  private eventTrigger: any;
  private root: any;
  private activeClass: any;
  private link: any;
  private popup: any;
  private closeButton: any;

  public constructor(element: any) {
    this.eventTrigger = Util.touch.isSupported ? 'touchend' : 'click' ;
    this.root = document.querySelector(element);
    this.activeClass = 'st-popup-active';
    this.link = this.root.querySelector('.st-root-link');
    this.popup = this.root.querySelector('.st-popup');
    this.closeButton = this.root.querySelector('.st-popup-close-button');

    this.link.addEventListener(this.eventTrigger, (evt: any) => {
      evt.stopPropagation();
      this.togglePopup();
    });

    this.popup.addEventListener(this.eventTrigger, (evt: any) => {
      evt.stopPropagation();
    });

    /* tslint:disable */
    this.closeButton && this.closeButton.addEventListener(this.eventTrigger, (evt: any) => {
        this.closeAllPopups();
    });
    /* tslint:enable */

    document.body.addEventListener(this.eventTrigger, (evt: any) => {
      /* tslint:disable */
      Util.touch.isDragging || this.closeAllPopups()
      /* tslint:enable */
    }, false);
  }

  private togglePopup() {
    const isActive = this.root.classList.contains(this.activeClass);

    this.closeAllPopups();
    /* tslint:disable */
    isActive || this.root.classList.add(this.activeClass);
    /* tslint:disable */
  }

  private closeAllPopups() {
    const activeLinks = document.getElementsByClassName(this.activeClass);

    for (const activeLink of activeLinks) {
      if (activeLink) {
        activeLink.classList.remove(this.activeClass);
      }
    }
  }
}
