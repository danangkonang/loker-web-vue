import { PaginationState } from '@/store/types';


export const defaultPagination: PaginationState = {
  count: 0,
  url: null,
};

/* createPagination - START */
// Description : createPagination function for serializer into pagination interface.
// Author : Komang Suryadana.
// Created on : Tue, 07 May 2019.       Updated on : Tue, 07 May 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

export function createPagination(params: any): PaginationState {
  const result = {
    count: 0,
    url: null,
  };
  const count = params.count;
  let url = params.next;
  if (url) {
    url = new URL(url);
    url = url.origin + url.pathname;
  } else {
    url = null;
  }

  result.count = count;
  result.url = url;

  return result;
}

/* createPgination - END */
