import 'izitoast/dist/css/iziToast.min.css';
import iZtoast from 'izitoast';

/* toast - START */
// Description : toast is for message info for make simple use iztoast.
// Author : Komang Suryadana.
// Created on : Tue, 29 January 2019.   Updated on : Tue, 29 January 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

const toast = {
  error: (message: any, title = 'Error') => {
    return iZtoast.error({
      title,
      message,
      position: 'bottomCenter',
    });
  },
  warning: (message: any, title = 'Warning') => {
    return iZtoast.warning({
      title,
      message,
      position: 'bottomCenter',
    });
  },
  success: (message: any, title = 'Success') => {
    return iZtoast.success({
      title,
      message,
      position: 'bottomCenter',
    });
  },
};

export default toast;

/* toast - END */
