import axios, { AxiosInstance } from 'axios';
import store from '@/store/index';
import router from '@/router';
import { baseAPI } from '@/functions/apibase.function';
import { requestHandler, responseHandler, errorResponseHandler } from '@/functions/axioshandler.function';

/* BaseService - START */
// Description : BaseService is base of all class service on here you can initialize interceptor request,
// response and error handling.
// The function interceptor will be execute first time before result pass into your service.
// Refresh JSON Web Token is also here.
// Author : Komang Suryadana.
// Created on : Tue, 29 January 2019.   Updated on : Tue, 29 January 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

// UNIVERSAL
const instance: AxiosInstance = axios.create({
  baseURL: `${baseAPI}landing/`,
});
instance.interceptors.request.use(requestHandler);
instance.interceptors.response.use(responseHandler, errorResponseHandler);

export function Base(): AxiosInstance {
  return instance;
}

/* BaseService - END */
