import { AxiosResponse } from 'axios';
import { Base } from './bases/base.service';

/* AcademyService - START */
// Description : AcademyService class of service for handling academy information.
// Author : Komang Suryadana.
// Created on : Tue, 23 April 2019.     Updated on : Tue, 23 April 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

export default class AcademyService {
  public serviceSchoolList(params: any): Promise<AxiosResponse<any>> {
    return Base().get('school/', {params});
  }
}

/* AcademyService - END */
