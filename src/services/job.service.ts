import { AxiosResponse } from 'axios';
import { Base } from './bases/base.service';

/* JobService - START */
// Description : JobService class of service for handling job infomation.
// Author : Komang Suryadana.
// Created on : Thu, 09 May 2019.       Updated on : Thu, 09 May 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

export default class JobService {

  public serviceSelectUnpagedJobCagetory(): Promise<AxiosResponse<any>> {
    return Base().get('category/');
  }

  public serviceSelectUnpagedJobTitle(): Promise<AxiosResponse<any>> {
    return Base().get('title/');
  }

  public serviceSelectVacancyList(params: any): Promise<AxiosResponse<any>> {
    return Base().get('jobs/', {params});
  }

  public serviceSelectJobVacancyById(id: number): Promise<AxiosResponse<any>> {
    return Base().get(`jobs/${id}/`);
  }

}

/* JobService - END */
