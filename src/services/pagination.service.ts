import { AxiosResponse } from 'axios';
import { Base } from './bases/base.service';

import { PaginationState } from '@/store/types';

/* PaginationService - START */
// Description : PaginationService class of service for handling pagination.
// Author : Komang Suryadana.
// Created on : Fri, 10 May 2019.       Updated on : Fri, 10 May 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

export default class PaginationService {

  public servicePaginate(url: string, page: number, params?: any): Promise<AxiosResponse<any>> {
    if (page) {
      return Base().get(`${url}?page=${page}`, {params});
    }
    return Base().get(url, {params});
  }

}

/* PaginationService - END */
