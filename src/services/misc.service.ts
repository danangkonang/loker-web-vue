import { AxiosResponse } from 'axios';
import { Base } from './bases/base.service';

/* MiscService - START */
// Description : MiscService class of service for handling misc infomation.
// Author : Komang Suryadana.
// Created on : Thu, 09 May 2019.       Updated on : Thu, 09 May 2019.
// Created by : Komang Suryadana.       Updated by : Komang Suryadana.
/* Version : 1.0:1. */

export default class MiscService {

  public serviceSelectMisc(): Promise<AxiosResponse<any>> {
    return Base().get('statistic/');
  }

  public serviceSelectUpagedCity(): Promise<AxiosResponse<any>> {
    return Base().get('city/');
  }

  public serviceSendMail(params :any) : Promise<AxiosResponse<any>>{
    return Base().post('contactus/',params);
  }

}

/* MiscService - END */
