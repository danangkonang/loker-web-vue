import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'landing',
      component: () => import('./views/Landing.vue'),
    },
    {
      path: '/candidate',
      name: 'candidate',
      meta: {
        group: 'candidate',
      },
      component: () => import('./views/Candidate.vue'),
    },
    {
      path: '/school',
      name: 'school',
      meta: {
        group: 'school',
      },
      component: () => import('./views/School.vue'),
    },
    {
      path: '/company',
      name: 'company',
      meta: {
        group: 'company',
      },
      component: () => import('./views/Company.vue'),
    },
    {
      path: '',
      component: () => import('./views/candidate/Job.vue'),
      children: [
        {
          path: '/job',
          name: 'job',
          meta: {
            group: 'candidate',
          },
          component: () => import('./views/candidate/job/JobList.vue'),
        },
        {
          path: '/job/:id',
          name: 'jobDetail',
          meta: {
            group: 'candidate',
          },
          component: () => import('./views/candidate/job/JobDetail.vue'),
        },
      ],
    },
    {
      path: '',
      component: () => import('./views/company/School.vue'),
      children: [
        {
          path: '/school-list',
          name: 'schoolList',
          meta: {
            group: 'company',
          },
          component: () => import('./views/company/school/SchoolList.vue'),
        },
      ],
    },
    {
      path: '/contactus',
      name: 'contactus',
      component: () => import('./views/ContactUs.vue'),
    },
    {
      path: '/tac',
      name: 'termandcondition',
      component: () => import('./views/TermAndCondition.vue'),
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue'),
    },
  ],
});
