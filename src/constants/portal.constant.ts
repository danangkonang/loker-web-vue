export const CANDIDAT_SIGNUP = 'https://career.support/candidate/#/signup';
export const CANDIDAT_SIGNIN = 'https://career.support/candidate/#/signin';

export const SCHOOL_SIGNUP = 'https://career.support/school/#/signup';
export const SCHOOL_SIGNIN = 'https://career.support/school/#/signin';

export const COMPANY_SIGNUP = 'https://company.career.support/signup';
export const COMPANY_SIGNIN = 'https://company.career.support/';
