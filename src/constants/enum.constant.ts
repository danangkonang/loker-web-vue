export enum ENUM_AGREEMENT_STATUS_TYPE {
  'Pending' = 1,
  'Approved' = 2,
  'Completed' = 3,
  'Rejected' = 4,
  'Terminated' = 5,
  'Expired' = 6,
}

export enum ENUM_JOB_VACANCY_STATUS {
  'Not Published' = 1,
  'Active' = 2,
  'Inactive' = 3,
}
